import Navbar from '../components/fixedComponents/Navbar'
import Footer from '../components/fixedComponents/Footer'
const Layout = props => (
    <div>
      <Navbar />
        {props.children}
      <Footer />
    </div>
  );
  
  export default Layout;