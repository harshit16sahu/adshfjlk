import React from 'react';
import {Row,Col} from 'antd';
import styled from 'styled-components'
import {Headings} from './LeftRightImages'



export default function ContactNavila() {
    return (
        <React.Fragment >
            
            <Row style={{padding:' 0 8%', background:'#f3f3f3'}}>
                <Col xl={12} md={24} style={{textAlign:'center'}}>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/> 
                    <br/>
                    <br/>
                  

                    <Headings fontsize="30px" fontweight="600">Schedule your appointment</Headings>
                    <br/>
                    <Headings fontsize="30px" fontweight="600">Call: +91-77609 66452</Headings>
                    <br/>
                    <Headings fontsize="26px" fontweight="600">Your beauty and self-care specialist</Headings>

                </Col>
                <Col xl={12} md={24} style={{textAlign:'center'}}>
                <br/>
                    <br/>
                   
                   <img src='/assets/images/services/navila.webp'
                        style={{borderRadius: '50%', border:'8px solid #d3d3d3'}}
                   />
                         <br/>
                    <br/>
                 
                </Col>
            </Row>
        </React.Fragment>
    )   

}
