import React from 'react';
import {Row,Col} from 'antd';
import styled from 'styled-components'

const PaddingRow = styled(Row)`
    @media (max-width: 1200px) {
        padding: 0 4.4% !important;    
    }
    
`
const Headings = styled.span`
    font-size: ${props=> props.fontsize};
    font-weight: ${props=> props.fontweight};
    padding-right: ${props=>props.padding};
    text-align: ${props=>props.textAlign} ;
    padding-left: ${props=>props.paddingLeft};
    padding-top: ${props=>props.paddingTop};
`
export {Headings};
const MobileAdjustments = styled(Col)`
    @media (min-width: 1200px){
        margin-top: 400px;
        padding-left: 100px;
    }
`

export default function LeftRightImages() {
    return (
        <React.Fragment>
            <PaddingRow>
                <Col xl={12} lg={12} md={24}>
                    <img src="/assets/images/services/services1.webp"
                        height= "500px"
                        width= "100%"
                    />
                </Col>
                <Col xl={12} lg={12} md={24} style={{marginTop: 80, paddingLeft: 70}}>
                    
                    <Headings fontsize="60px" fontweight="800" style={{marginTop: 50}}>Our Courses</Headings>
                    <br/>
                    <Headings fontsize="30px" fontweight="400">Our professional beauty team will care of you</Headings>
                    <br/>
                    <br/>
                    
                    <ul>
                        <li style={{fontSize: 20}}>Private One-On-One Makeup Course (Basics)</li>
                        <li style={{fontSize: 20}}>Makeup &amp; Hair Workshop (Basics)</li>
                        <li style={{fontSize: 20}}>Hair Grooming</li>
                        <li style={{fontSize: 20}}>Pro Makeup and Basic HairStyling</li>
                        <li>
                       <p><span style={{fontSize: 20}} class="uavc-list-desc ult-responsive">Private One-On-One Makeup Course (Basics)</span></p>
                         </li>
                     </ul>
                </Col>
            </PaddingRow>


            <PaddingRow>
                
                <Col xl={12} lg={12} md={24} style={{marginTop: 80, paddingLeft: 70}}>
                    
                    <Headings fontsize="60px" fontweight="800" style={{marginTop: 50}}>Our Stylists</Headings>
                    <br/>
                    <Headings fontsize="30px" fontweight="400">Our professional beauty team will care of you</Headings>
                    <br/>
                    <br/>
                    
                    <Headings fontsize="20px">
                        <p style={{paddingRight: 40, textAlign: 'justify'}}>
                        Meet Your Stylist is designed to match Stylists with new and/or existing clients that are the best fit for them. This goes beyond their talent behind the chair. This reaches the inner core of each person. Meet Your Stylist was created to benefit the Stylist’s choice of clients versus the other way around. Meaning, the professionals choose who they do their best work with. And because we’re creative, emotional humans, this custom survey syncs them with clients that are the best fit based on personality styles and RELATIONSHIP preferences!
                        </p>
                    </Headings>

                </Col>

                <Col xl={12} lg={12} md={24}>
                    <img src="/assets/images/services/services2.webp"
                        height= "500px"
                        width= "100%"
                    />
                </Col>
            </PaddingRow>
        </React.Fragment>
    )   

}
