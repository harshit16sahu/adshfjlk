import React, { Component } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
display:flex;
justify-content:space-between;
padding: 0 8%;
flex-wrap: wrap;
`

const LeftWrapper = styled.h1`
    font-size: 6rem;
    font-weight: 600;
    display:flex;
    flex-direction: column;
    
    margin-left: 6rem;
    color: ${props=> props.theme.colors.main };

`
export default class HomepageSliderWithData extends Component {
    render() {
        return (
            <React.Fragment>
                <Wrapper>
                    <LeftWrapper>
                        <br/>
                      
                        <img src="https://bhavyata.com/wp-content/uploads/2019/06/er.png"
                            height="50"
                            width="100"
                            style={{marginTop: 30}}
                        />
                        
                    </LeftWrapper>
                    
                    <img src={this.props.slider.imageURL}
                        height= "500"
                        width = "800"
                        alt=""
                        style={{
                            borderRadius:  "150px 0"
                        }}
                    />
                
                </Wrapper>
                

            </React.Fragment>
        )
    }
}
