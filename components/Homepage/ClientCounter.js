import React, { Component } from 'react'
import {Row, Col} from 'antd';
import {CenterCol} from './Homepage';
import CountUp from 'react-countup';
import styled  from 'styled-components'

const IncreseCounterFont = styled(CountUp)`
    font-size: 6rem;
`

const HeadingColor = styled.h1`
    color: rgb(114, 68, 141);
    font-weight:900;
`

export default class ClientCounter extends Component {
    state={
        clientNumber:0,
        coursesOfferd: 0,
        followers:0
    }

    render() {
        return (
            <div>
                <Row>
                    <CenterCol xl={8} lg={8} sm={24} xs={24} >
                    <IncreseCounterFont
                        start={0}
                        end={1200}
                        duration={15}                        
                        />
                        <HeadingColor>Satisfied Clients</HeadingColor>
                    </CenterCol>
                        
                    <CenterCol xl={8} lg={8} sm={24} xs={24} >
                    <IncreseCounterFont
                        start={0}
                        end={9}
                        duration={10}
                        
                        />
                        <HeadingColor>Courses Offerd</HeadingColor>
                    </CenterCol>


                    
                    <CenterCol xl={8} lg={8} sm={24} xs={24} >
                    <IncreseCounterFont
                        start={0}
                        end={1250}
                        duration={15}
                       
                        />
                        <HeadingColor>Followers</HeadingColor>
                    </CenterCol>
                </Row>
            </div>
        )
    }
}
