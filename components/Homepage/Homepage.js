import React, { Component } from "react";

import { Row, Col } from 'antd';    
import styled from 'styled-components';
import Slider from 'react-slick';
import OurServices from './OurServices'
import OurCourses from './OurCourses';
import ClientCounter from "./ClientCounter";


const Image = styled.img`
    width: 100%;
    @media (min-width: 1200px) {
        height: 600px;
        
    }
    @media (max-width: 1200px) {
        height: 350px;
    }
`
const Wrapper1 = styled(Row)`
    display:flex;
    flex-wrap: wrap;
    align-items: center;
`
const Bodydiv = styled.div`
    padding: 5% 5%;
`
const RoundImage = styled.img`
    height: 150px;
    width: 150px;
   
    border-radius: 50%;
    @media (min-width: 1200px){
        height: 250px;
    width: 250px;
    }

`
const CenterCol = styled(Col)`
    text-align:center;
`

const DesktopViewHeading = styled.h3`
    color: grey;
    @media (max-width: 1200px) {
        display: none;
    }
`
const BackroundColor = styled.div`
    background-color: #f3f3f3;
`
export {CenterCol};

class HomePage  extends Component {
    
    render() { 
        const settings = {
            dots: true,
            arrows:false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return ( 
    <BackroundColor>
        <div style={{padding: 0,}}>
            <Slider {...settings}>
                    <div>
                       <Image src="/assets/images/backgroundImage3.webp"/>
                    </div>
                    <div>
                    <Image src="/assets/images/backgroundImage2.webp"/>    
                    </div>
                    
            </Slider>
         </div> 
       
        
           
           <Bodydiv>
                <Col style={{textAlign:'center'}}>
                    <strong><h1>Come learn at the best Makeup Academy in Bangalore!</h1></strong>
                </Col>
                <br/>
               <Row type="flex" justify="end">
                    <CenterCol xl={6} lg={6} sm={12} xs={12}>
                        <RoundImage src="/assets/images/bridal-service.jpg" ></RoundImage>
                        <h2>Bridal Services</h2>                       
                        <DesktopViewHeading >Navilaz goal in weddings is to create a warm, naturally beautiful look. </DesktopViewHeading>
                    </CenterCol>
                    <CenterCol xl={6} lg={6} sm={12} xs={12}>
                        <RoundImage src="/assets/images/hair-styling.jpg" ></RoundImage>
                        <h2>Hair Styling</h2>
                        <DesktopViewHeading >We help people to look and feel their best in them. </DesktopViewHeading>
                    </CenterCol>
                    <CenterCol xl={6} lg={6} sm={12} xs={12}>
                        <RoundImage src="/assets/images/makeup-course.jpg" ></RoundImage>
                        <h2>Advanced Makeup Course</h2>
                        <DesktopViewHeading >You will learn advanced professional techniques.</DesktopViewHeading>
                    </CenterCol>
                    <CenterCol xl={6} lg={6} sm={12} xs={12}>
                        <RoundImage src="/assets/images/nail-cutting.jpg" ></RoundImage>
                        <h2>Nail Cutting and Styling</h2>
                        <DesktopViewHeading>Nail Styling is very important to beautify your appearance.</DesktopViewHeading>
                    </CenterCol>

               </Row>



               {/* Our Services Section */}
<br/>
              <OurServices />
              <br/>
              <OurCourses />  
              <br/>
              <ClientCounter />
           </Bodydiv>           
       </BackroundColor>
    
         );
    }
}
 
export default HomePage ;