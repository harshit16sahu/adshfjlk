import React from 'react'
import Slider from 'react-slick';
import styled from 'styled-components';
import {Card, Row, Col} from 'antd';
const { Meta } = Card;
const Image = styled.img`
    text-align:center !important;

    &:hover{
        padding: 10px;
        transition: 0.5s;
    }
    @media (min-width: 1200px) {
        width: 12rem !important;        
        height: auto !important;
    }
    @media (max-width: 1200px) {
        height: auto;
        
    }
`
const RemoveMetaDescription = styled(Meta)`
   
    @media (max-width: 1200px) {
        .ant-card-meta-description{
            display:none
        }
    }
`

const Middle = styled(Card)`
    display:flex;
    justify-content:center;
    flex-wrap:nowrap;
    text-align:center !important;
    @media (max-width: 1200px) {
      flex-wrap:wrap;
}
`

export default function OurCourses() {
    const settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };
   
    return (
        <div>
            
             <center>
               <h1>Our Courses</h1>
               <h3 style={{color:'grey'}}>We offer a full range of beauty services to everyone located in the area. Our professionals know how to handle a wide range of treatments.</h3>
               </center>
             
               <Slider {...settings}>
          <div>
                <Row type="flex" justify="center">
                  <Col lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/bridal-makeup.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Bridal Makeup" description="Your wedding day is the most important day of your life!

Let the professionals at NavilazStudio assist you in helping you look and feel your best. Begin your “happily ever after” from head to toe by ensuring you cover each specific detail."
                        />
                    </Middle>
                  </Col>
                  <Col  lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/professional-makeup.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Professional Makeup" description="The professional makeup courses at Navilaz Studio ranges from 4 to 12 weeks and include all professional makeup disciplines from beauty makeup to advanced creative makeup as well as fashion, bridal, airbrush makeup courses."
                        />
                    </Middle>
                  </Col>
              </Row>
            
            <br />
            <Row type="flex" justify="center">
                  <Col lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/advanced-hairstyle.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Advanced Hairstyling" description="This course concentrates on the Advanced hairstyling, from simple blow-drying techniques to creating elaborate hairstyles. You will learn to create modern styles, as well as edgy and creative hair designs, using hot tools and roller sets. This course incorporates daily projects and skill-building sessions to encourage learning by practice."
                        />
                    </Middle>
                  </Col>
                  <Col  lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/air-brush.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Professional Airbrush" description="Airbrushing has become one of the premier ways of applying make-up. In this course, the airbrush technique is de-mystified. You will be shown how to properly care for and maintain your airbrush. In addition, students will study the techniques necessary to do beautiful and flawless make-up.
"
                        />
                    </Middle>
                  </Col>
              </Row>
          </div>
          <div>
          <Row type="flex" justify="center">
                  <Col lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/nail-paint.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Nail Paint" description="A team of professional nail technicians provide hands-on training on various nail art courses and prepare you as an expert in the area of nail art. You will be equipped with the latest nail trends and designs. You will also be given complete training and conceptual understanding of all the techniques and processes in nail art."
                        />
                    </Middle>
                  </Col>
                  <Col  lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/personal-grooming.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Personal Grooming" description= "Personal grooming courses at Lakmé Academy train you in specific beauty and fashion skill sets, like makeup, saree draping, and hair styling. The duration of personal grooming courses at Navilaz Studio ranges from 4 days to 12 days, depending upon course type." 
                        />
                    </Middle>
                  </Col>
              </Row>
            
            <br />
            <Row type="flex" justify="center">
                  <Col lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/beauty-therapy.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Beauty Therapy" description= "Be a qualified beauty therapist and make your place in the fashion and glamour industry.On completion of the course you can work as an independent beauty therapist or in high-end salons." 
                        />
                    </Middle>
                  </Col>
                  <Col  lg={12} xl={12} sm={12} md={12} xs={12} >
                  <Middle hoverable
                    cover={<Image alt="example" src="assets/images/skin.jpg"></Image>}
                    >
                    <RemoveMetaDescription title="Skin" description="A perfect mix of theory and practice on latest skin care products and skin treatments makes you a skin care expert. Join skin care course at Lakmé Academy and get trained to give the much-desired aesthetic look to clients." 
                        />
                    </Middle>
                  </Col>
              </Row>
          </div>
         
        </Slider>
        </div>
    )
}
