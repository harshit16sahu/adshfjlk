import React from 'react'
import { Card } from 'antd';
import {Row, Col} from 'antd';
import styled from 'styled-components';
import {CenterCol} from './Homepage';


const { Meta } = Card;
const Image = styled.img`
    border: 2px solid;
`
const RemoveMetaDescription = styled(Meta)`
 .ant-card-meta-description{
            display:none
        }
    @media (max-width: 1200px) {
        .ant-card-meta-description{
            display:none
        }
    }
`


export default function OurServices() {
    return (
        <div>
             <center>
               <h1>Our Services</h1>
               <h3 style={{color:'grey'}}>We offer a full range of beauty services to everyone located in the area. Our professionals know how to handle a wide range of treatments.</h3>
               </center>
               <Row>
                   <CenterCol xl={6} lg={4} sm={12} xs={12} md={12}>
                    <Card
                            hoverable
                            
                            cover={<Image alt="example" src="assets/images/bridal-service.jpg" />}
                        >
                            <RemoveMetaDescription title="Bridal Makeup" description="Your wedding day is the most important day of your life!

                                Let the professionals at NavilazStudio assist you in helping you look and feel your best. Dive in." />
                        </Card>
                    </CenterCol>
                    <CenterCol xl={6} lg={4} sm={12} xs={12} md={12}>
                    <Card
                            hoverable
                            
                            cover={<Image alt="example" src="assets/images/nail-cutting.jpg" />}
                        >
                            <RemoveMetaDescription title="Nail Art" description="Our professionals know how to handle a wide range of beauty treatments. Whether you want for your beauty care we will offers using the best products and techniques." />
                        </Card>
                    </CenterCol>
                    <CenterCol xl={6} lg={4} sm={12} xs={12} md={12}>
                    <Card
                            hoverable
                            
                            cover={<Image alt="example" src="assets/images/makeup-course.jpg" />}
                        >
                            <RemoveMetaDescription title="Advanced Courses" description="Our professionals know how to handle a wide range of beauty treatments. Whether you want for your beauty care we will offers using the best products and techniques." />
                        </Card>
                    </CenterCol>
                    <CenterCol xl={6} lg={4} sm={12} xs={12} md={12}>
                        <Card
                            hoverable
                            cover={<Image alt="example" src="assets/images/skin-care.jpg" />}
                        >
                            <RemoveMetaDescription title="Skin Care" description="Our professionals know how to handle a wide range of beauty treatments. Whether you want for your beauty care we will offers using the best products and techniques." />
                        </Card>
                    </CenterCol>
               </Row>

        </div>
    )
}
