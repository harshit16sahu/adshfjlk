import React from 'react'
import { Collapse } from 'antd';

const { Panel } = Collapse;
export default function Accordion(props) {
    return (
        <Collapse >
            <Panel header={props.header}>
            <p>{props.text}</p>
            </Panel>
         </Collapse>
    )
}
