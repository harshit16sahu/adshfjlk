import React from 'react';
import {Row,Col} from 'antd';
import {Headings} from '../Services/LeftRightImages'
import { Input,Button } from 'antd';
const { TextArea } = Input;

export default function Footer() {
    return (
        <React.Fragment>
            <Row style={{padding: '2% 12%', background:'#181818', color:'white'}}>
                <Col xl={6} style={{textAlign:'center'}}>
                    <Headings fontsize="1.5rem" fontweight="700">About us</Headings>
                    <br/>
                    <br/>
                    <Headings fontweight="600">
                        <p style={{textAlign:'justify'}}>
                        Navilaz Academy n Studio delivers aesthetically designed makeup services to make women feel beautiful in their own skin. If you feel comfortable in your skin, then you can spend your energy loving others and enjoying life instead of worrying and being self-conscious. Is makeup the best way to gain self confidence? No way! But it can help a little, plus it’s FUN! Join Navilaz Academy and together let’s make life be a little bit happier, and perhaps more colorful.
                        </p>
                    </Headings>
                </Col>
                <Col xl={6} style={{textAlign:'center'}}>
                    <Headings fontsize="1.5rem" fontweight="700">Find our Studio</Headings>
                        <br/>
                        <br/>
                    <Headings fontweight="600">
                    <Col span={6}></Col>
                        <Col span={4} style={{textAlign:'left'}}>
                        <span style={{fontWeight:'bold'}}>Call:</span>
                        </Col>
                        <Col span={14} style={{textAlign:'left'}}>
                            <span style={{paddingLeft:4}}></span>+91 77609 66452<br/><br/>
                    </Col>
                    <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Timings:</span>
                </Col>
                <Col span={14} style={{textAlign:'left'}}>
                    <span style={{paddingLeft:4}}></span>10am-5:30pm<br/><br/>
                </Col>

                
                <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Address:</span>
                </Col>
                <Col span={14} style={{textAlign:'justify'}}>
                    <span style={{paddingLeft:5}}></span>3rd Floor, 27th Main Road,<br/> HSR Layout, Sectior 1,<br/> Banglore 560102<br/><br/>
                </Col>
                

                <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Email:</span>
                </Col>
                <Col span={14} style={{textAlign:'left'}}>
                    <span style={{paddingLeft:4}}></span>contact@navilazstudio.com<br/><br/>
                </Col>


                    </Headings>
                </Col>
                <Col xl={6} style={{textAlign:'center'}}>
                <Headings fontsize="1.5rem" fontweight="700">Contact us</Headings>

                <Col style={{textAlign:'center'}}><br/><br/>
                    <Input placeholder="First Name" style={{width:"40%", marginRight: "3%",marginBottom:"3%"}}/>
                        <Input placeholder="Last Name" style={{width:"40%"}}/><br/>
                        <Input placeholder="Email" style={{width:"83%", marginBottom:"3%"}}/><br/>
                        <TextArea placeholder="Message" rows={4} style={{width:"83%"}}/><br/>
                        <br/><Button type="primary" block  style={{width:"83%"}}>Send</Button>
                </Col>
                </Col>
                <Col xl={6} style={{textAlign:'center'}}>
                <Headings fontsize="1.5rem" fontweight="700">Our Location</Headings>

                    <Col style={{textAlign:'center'}}><br/><br/>
                    <iframe 
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.9303940286686!2d77.64947031460282!3d12.91219521967317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae149d3cffed67%3A0x5ee43d11b4411523!2sINIFD+Bangalore+HSR+Layout!5e0!3m2!1sen!2sin!4v1548012056385" 
                        frameborder="0" 
                        style={{border: '0px', pointerEvents: 'none'}} 
                        allowFullScreen=""></iframe>
                    </Col>
                </Col>
            </Row>
        </React.Fragment>
    )
}
