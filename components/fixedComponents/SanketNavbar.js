import React, { Component } from "react";
import { Layout } from 'antd';
import './Navbar.css'
import { Row, Col } from 'antd';    
import styled from 'styled-components';
import { Menu, Icon } from 'antd';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
const { SubMenu } = Menu;
const Insidediv = styled.div`
    padding:0% 6%;
`
const Wrapper1 = styled(Row)`
    display:flex;
    flex-wrap: wrap;
    align-items: center;
   
`
const Image = styled.img`
    width: 100%;
`

const Links = styled(Link)`
    color: red;
    font-weight: 500;
    

`


class Navigation extends Component {
    state = {
        current: 'mail',
      };
    
      handleClick = e => {
        console.log('click ', e);
        this.setState({
          current: e.key,
        });
      };
    render() {
               return ( 
        <div>
            <Insidediv>
                <Wrapper1>
                    <Col lg={4} xl={4}  xs={24} sm={24} md={24} style={{textAlign:'center'}}>
                        <img src={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAjVBMVEX///8jHyAAAAAYExRta2seGhv8/Pycm5sOBgl0c3RJRkejoqKzsrL39/cgHB0IAAC9vb3ExMTh4eGNi4vx8fHNzMzq6uoUDhCpqKgaFReWlZWCgYEOBQjU1NTa2tqgn580MTJBPj8oJCVSUFBeXF1ubG23trZ4d3cvKyxaWFlNSks8ODmOjY1XVFVmY2TGrz0WAAAL60lEQVR4nO1dDXeqOBA1oYgWCYoIovjRVmtbff7/n7cJKCSBBFAQ2JN7zp59WtC5EDJzJzNxMFBQUFBQUFBQUFBQUFDoHKy2DWgas4+2LWgYbggvbdvQLK4+gNu2jWgSSwiAvh+2bUZz8CACAIxObdvRHP40QAC/2zakKaztiCBAtte2Kc1gA8EN2qFtWxqB9anfGQK4bNuaJnAcgRTQaduc+jGFFEEQvLdtT+3wbEQzBPa6bYvqxj8NsICLtk2qFwbkCAL9023bqDqxzRAEwH9r26o68R5kGQI4bdus+nAPZniK/5vQZpYzRv9XLsMNUD5DAI22basHB19AEFPctG1cHZgIxmg0Tj/+B2qYD2ZYjPof2gw/8hwFNU57r4Yv+Y4iAfJ7HtosJA9hDL/fatj60YsY9lwNr8NCggD1ObT5LhyjBD12GS6UOYoU9rxtSx/FL696ReirGl6VGqMEOuily3AKPCGNXib6h7mqVzhOJ23bWx0C1SsA6l8CVaR6RdD+tW1xRYhVrwh9U8NXseoVUuzV2rBM9YrQq9DGKRnMsAh75DIqOYoU/UmgXh4YowQI9SS02TxIsDdq2NoXq14R+hHanEqoXhF6EdqUU70iBOe27S+E5z/iKFJ0v+ztq6zqFVLseKLffGqMEug/na5AdZ4miNXwuG0WEhSl8MuhywnUOVa9SOcgmXn4QwnwJ4w6m0Alqhft/94Z/InT3p/coeToP9BhNRwVroUm/7ZQaKAc3zcc/uCj4e4V9lZHXLim2R/jKTMdTkXTD4I/1x01Ip3LRxhfjm6GNonq1TXoz+i/zIUzLAogXN2OssYwvA9o/aeDatijByNipd4/WU4DHuODznQ8G3ZwbZh1FOyCkvspkxuxdzix2cfuqWFe9bJZbKlkRMAiQ4B/s2NqOEMBfTKzTbZyj7tfS/6A0bEtLrnIUb1cBC1LL/rzweAtE7F3K7RZjzJmY7m+o27jUPIoBofB8Jz5c6fUcJ7HC43B6kodwz9oFPR3HC1kw4KgO01guaoXDz0j8XUE4iwx2g/cvHUc2Jm14dzCNf80MEJIe/6TaDUKfQ7z73BX1HC+6tXGA2OEdNot/gn0P9IFDDuihgWqN2IItHcq/PJQfgyOGQo+xO+Eyzjnq96YIdt1IFhUFDPsRNmbaK33xpB1a/nFCxKGKGxdDQsL1+4M2XXB37xJScKwfTUsLlxLGDKlJC7KOV7GEMCMpH4tjtlghmcIfPo2bHMUv5Rhyy5DstabMmTz2JkQu4ih/tMaPXmFM8WQnRGzjl/OsNUmMFl6lGbIBNHDjHspYNiiGpaKPpohCM6U488k34oYclrzdZCv9TIMwYhu4+IdfxFD4F+FRjSJoXytl2UIGJnBpTwKGbakhnNUr4QhW0PKrsEVM0R+C2q4aK2XZ4h8Kv5yGUVZzLANNezKVlzyGALtizqdeRRLMGRH+UuQG2BKGQJI+zVaVJZh+PLQpnitN8uQDTGPaYq7FEMdvNRlOGFhPUIOQ2RTMsPaJ46/FEOSFnkh9sVrvTkM2ayEk4R85Ri+tJyoTIVzHkO2rCsJ20syZGbjZlHcriViyE6J9xi8JMPXhTaWdCVJzpCNom/Jt7IMX7Y2fJIHM3KGyKaik5v6Ks3wRZXSJSucBQzZYufY8Zdn+JJdtEo4CilDENIVQVHyrTzDl6hhUeK6NENWKJDYqALDFzSBSVVvOYaMlWTlsQrDxtVw3iYllRkyaV78iVUYNu4yylc4SxiyMmMJKzFsWA2XcxSFDFmZMQ4rMWT8Td0QVjdVZcg4/uHer8IQaH+NEfRylqIfZMjIDGdUiWGDaliySUlVhuw2Ud9WtdLbplxGtXatAoZgxMyJ1Rjqn40QrNiuVcSQbTisWD7dTDlRiQ0gKjEEdBlD1QLxJtaG55X6erFnXg+W8u4ZurqvKkOk1V72Vkr10hhdClOqQaoUKhf5194ENqw4RrEK2BV3O9tJcql6G0Pda8MVgpm7BbMSZiePU3WGjEd9Hg/09YY4tiqWkvc87wOtKMG+RoLeqHK7lk4WGrIllZnDbmUMjzTb1LklSlnVSyEKPEtoLf/rYYaMu3kOuwfGaDzVrYvPjPMSDzFEek1quLzqTb4a/t2+ewwL5+Dobj/WElaTGi6zmxWFwIfwJxWp0zOEvnSQI5IifLDprR41XGY3K4rg4TJlFao3XR2lyxwkRSipIJahliawipuU+OvZdDr9xkj/N1vKZZd/mJoPtkcz+ZDHULjWmzEXZlEU0mrw4f7v59XwW/VNSl4L+8m14ZwqtI5Bfy6BupVuzdkNPNc3XEtfb9N4JrQRNw52CSh8WA1XVr0tQft9kGB11dsWHlXDV6j1BY8lUDeHt97g2sGmWgUFBQUFBQWFPDSyi4RrLicYaT7rO3q9ZJcnl7TI3i6jQ/BBZrxysohPmQ025v0v5IRpclx+U9OGT06YNoreWsQfESP691PUveknhHCfUHA3bxBeN0zeYMOsxw7jUyC8OPFR1nYP4XnmDizHiP5A/k3ePkavjtv8JMSea7DwINKiH94hpx2Ws41DMCHWPVmuSFK1Ib1qfuarWU4h9w7ZgCCgstFT+HFjYYQApL/u+KeDQKTstpDrPnChrmvkY67wPc3HnnUUPl0/9O6zqeU5V6frhpkNLNYjssadwEnqXj1IczJDcXH63Pc5uWD+7COGX1T/+8WuY4PlMSmEoi4o/9wsYebHYggRKlM7TbpkrZCmjj9YlGIZQpRdqV9Go/SczgobWEu30JFUuep68nUTjuFh7aOAe5aOGv17XOuEBmGYdszOoLD0ZzL3gc2vSbxHSp7KrH3otZRFj63BdURlzzmGmzm+Y/xgW0AQprno9DJbPvLTZU0Jw/fBb6Dv2fe+o1topV+/g/XUf52saPUp6VbmGJ62g69A58fKXteTrtFZ+qRYWjmGzik7hCdf3EDZ4qFVy26nR4sMeJR8H8vQ+hf16fFtV4advnVNx2tZhvMN2bBGo1cHzcyc9BXUVN92IpduAgHSYktZht/L2G4u74xH7r1NyaPsLMuQjMf5SP7begasa8uzYzQ41va9jYBleCBGrH0w4ia+f8F9+lnRRd3lGEbjGjtiW9JA4pBrXk9rwjg29Eu71fcwDOMbhGdtfjGWjNzowCG9wFeS4VvkX38DJClB/AtqK1S4MbRGKKbBMLzEFuI7xm18YOko9u1Tmno5hl5c5SBxl7EXrqtb73ibwrAHiMqQGIbAjKJn8qv33Fxzuo3cN3oGLMdweZ2QkNqEQBP9VCl+zuvbwP10NxE/2gGOwWmGi/ViFgFPfFzF5yb2kg4zGZRj+C/+zAW+SKINzfAzU99GC8fkJoxDUhFIMzzcJzs810Dukr7rOp4S50zMUYrh4n5R8GQyyi97qnOMJs/hIN4lGJrTlKGVzCLY+4ZcCGzaZLZnayTlDN34m07JRfnCj3eetvJCxPzk3pO9pSnDgUNmm3XKkPLCxBh27nZ9FBoLdorNMqSnEiMakulliybkPOt/NaZG2H3ydr5RFyuSGalR1HYeE5h5ME6+/v7G3gISeYsZ/kVjfpJyGo5QkNNbgYkzK4bLJ4PTK/18zWmjNpSrw3cs4HZ0wpMvXz3IqidsqU0t/Hnxk7ynJhfyeGfEg4unUTraGdpPjtI9Ezpd/ZThL7110JeecRhnnQ+7MgqYlpHHiOEMUsfja5D9betfnxm7w+uTnt9jnwTrM/m8CaRv2h5lFpsvkK/FXtoApUrzENDZgxUkZSPWD+0gSKUUf90wa3q0eL/hU7stuIs9hBOPmkO2N4buDtrwcrtHljOGtm3D64Ye0g57cYfeMjroI0pkDZ01eWXDt4XjOZvlB4SBZW3w133cEl1Dbxodb3/T3+9B/L0Lj8DZLowDefmECnZ3q51prgz6Is2ia+wahmmaxiqmuFyRV6aJD6cnVHYi3Rj3g0ySTbydgl8aOwP/Z5pL/OaOvBFPWdvVKj5itdrSnxKdRGCsotNMoxObKyooKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKJTGf22tzWfXV3uHAAAAAElFTkSuQmCC'}
                        height="100"
                        width= "100"/>
                    </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8}></Col>
                       <Router>
                       <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
                       <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                            <Links to="/home" >Home</Links>
                        </Col>
                        <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                            <Links to="/gallery" >Gallery</Links>
                        </Col>
                        <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                            <Links to="/testimonials" >Clients</Links>
                        </Col>
                        <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                            <Links to="/services" >Services</Links>
                        </Col>
                        <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                            <Links to="/contact" >Contact</Links>
                        </Col>
                        <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                            <Links to="/Our Courses" >Courses</Links>
                        </Col>
                        </Menu>
                       </Router>

                </Wrapper1>
            </Insidediv>              
        </div>

         );
    }
}
 
export default Navigation;