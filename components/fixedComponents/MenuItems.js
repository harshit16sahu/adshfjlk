import React from 'react'  
import styled from 'styled-components' ;
import { Menu, Icon } from 'antd';
import Link from 'next/link';

const Links = styled(Link)`
font-size: 1rem;
font-weight: 600;
&:hover{
    color: red !important;
    border-bottom: hidden;
}
`
const MenuItem = styled(Menu.Item)`
    border-bottom: hidden !important;
`

export default function MenuItems() {
    return (
        <React.Fragment>
                            {/* <MenuItem>
                                <Links to="/home" >Home</Links>
                            </MenuItem>
                            <MenuItem>
                                <Links to="/gallery" >Gallery</Links>
                            </MenuItem>
                           <MenuItem>
                                <Links to="/testimonials" >Clients</Links>
                            </MenuItem>
                           <MenuItem>
                                <Links to="/services" >Services</Links>
                            </MenuItem>
                           <MenuItem>
                                <Links to="/contact" >Contact</Links>
                            </MenuItem>
                           <MenuItem>
                                <Links to="/Our Courses" >Courses</Links>
                            </MenuItem> */}
                            <MenuItem>
                                <Links>
                                    <a href="/gallery">Gallery</a>
                                </Links>
                            </MenuItem>
        </React.Fragment>
    )
}
