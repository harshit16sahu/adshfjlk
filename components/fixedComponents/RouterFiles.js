import React from 'react'
import { Switch,Route} from 'react-router-dom';
import HomePage from '../Homepage/Homepage';
import OurServices from '../Homepage/OurServices';
import Contact from '../Contact';
import OurCourses from '../Homepage/OurCourses';
import Testimonials from '../Testimonials';
import Gallery from '../Gallery'

export default function RouterFiles() {
    return (
        <div>
            <Switch>
                <Route exact path="/"><HomePage /></Route>    
                <Route exact path="/gallery"><Gallery /></Route>    
                <Route exact path="/testimonials"><Testimonials /></Route>    
                <Route exact path="/services"><OurServices /></Route>    
                <Route exact path="/contact-us"><Contact /></Route>    
                <Route exact path="/courses"><OurCourses /></Route>    
            </Switch>            
        </div>
    )
}
