webpackHotUpdate("static\\development\\pages\\courses\\professional-makeup.js",{

/***/ "./components/courses/Accordion.js":
/*!*****************************************!*\
  !*** ./components/courses/Accordion.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Accordion; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
var _jsxFileName = "C:\\Users\\harsh\\Desktop\\learning\\nextjs-navilaz\\components\\courses\\Accordion.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var Panel = antd__WEBPACK_IMPORTED_MODULE_1__["Collapse"].Panel;
function Accordion(props) {
  return __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Collapse"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, __jsx(Panel, {
    header: props.header,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, __jsx("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, props.text)));
}

/***/ })

})
//# sourceMappingURL=professional-makeup.js.cf34b6fa3f77d0738ff5.hot-update.js.map