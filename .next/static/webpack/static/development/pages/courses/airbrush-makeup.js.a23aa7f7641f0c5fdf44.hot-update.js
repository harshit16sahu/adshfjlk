webpackHotUpdate("static\\development\\pages\\courses\\airbrush-makeup.js",{

/***/ "./pages/courses/airbrush-makeup.js":
/*!******************************************!*\
  !*** ./pages/courses/airbrush-makeup.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return professionalMakeup; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Layout/Layout */ "./Layout/Layout.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/Services/LeftRightImages */ "./components/Services/LeftRightImages.js");
/* harmony import */ var _components_Homepage_ClientCounter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/Homepage/ClientCounter */ "./components/Homepage/ClientCounter.js");
/* harmony import */ var _components_courses_Accordion__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/courses/Accordion */ "./components/courses/Accordion.js");
var _jsxFileName = "C:\\Users\\harsh\\Desktop\\learning\\nextjs-navilaz\\pages\\courses\\airbrush-makeup.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






function professionalMakeup() {
  return __jsx(_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    style: {
      padding: '0 8%',
      textAlign: 'center',
      background: '#f3f3f3'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    xl: 8,
    lg: 12,
    md: 24,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, __jsx("img", {
    src: "/assets/images/courses/airbrush-makeup.jpg",
    width: "100%",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  })), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 12,
    md: 24,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, __jsx(_components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__["Headings"], {
    fontsize: "2.4rem",
    paddingLeft: "2rem",
    fontweight: "700",
    paddingTop: "4rem",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, "Professional Airbrush Makeup"), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }), __jsx(_components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__["Headings"], {
    fontsize: "2rem",
    paddingLeft: "2rem",
    fontweight: "600",
    paddingTop: "4rem",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "About the Course"), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }), __jsx("p", {
    style: {
      textAlign: 'justify',
      marginLeft: '2rem',
      fontSize: '1.4rem',
      fontWeight: '600'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "Airbrushing has become one of the premier ways of applying make-up. In this course, the airbrush technique is de-mystified. You will be shown how to properly care for and maintain your airbrush. In addition, students will study the techniques necessary to do beautiful and flawless make-up. Learn the techniques that are being required by major studios around the world as well as clients."))), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    style: {
      padding: '0 18%',
      textAlign: ''
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, __jsx(_components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__["Headings"], {
    fontsize: "2rem",
    fontweight: "600",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, "Some Frequently Asked Questions about this course."), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }), __jsx(_components_courses_Accordion__WEBPACK_IMPORTED_MODULE_5__["default"], {
    header: "What is the duration of the course?",
    text: "The Duration of this course is 36 days.\r The timings will be Monday to Saturday 2:00pm-3:00pm",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: this
  }), __jsx(_components_courses_Accordion__WEBPACK_IMPORTED_MODULE_5__["default"], {
    header: "Will I get a Certificate for this Course?",
    text: "Yes, you will be receiving a diploma certificate from us which is Internationally recognized. Absolutely worth your time and effort, the certificate will provide you with International recognition where you can showcase your talent in Makeup.",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }), __jsx(_components_courses_Accordion__WEBPACK_IMPORTED_MODULE_5__["default"], {
    header: "Do you offer professional photo-shoot for students",
    text: "Yes, 2 professionally shot portfolio shoots, from which students can start building their portfolio. The photoshoot will immensely impact your career and our trained photographers will be working alongside you.",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }))));
}

/***/ })

})
//# sourceMappingURL=airbrush-makeup.js.a23aa7f7641f0c5fdf44.hot-update.js.map