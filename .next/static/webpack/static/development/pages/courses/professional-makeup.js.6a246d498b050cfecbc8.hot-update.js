webpackHotUpdate("static\\development\\pages\\courses\\professional-makeup.js",{

/***/ "./pages/courses/professional-makeup.js":
/*!**********************************************!*\
  !*** ./pages/courses/professional-makeup.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return professionalMakeup; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Layout_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Layout/Layout */ "./Layout/Layout.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/Services/LeftRightImages */ "./components/Services/LeftRightImages.js");
var _jsxFileName = "C:\\Users\\harsh\\Desktop\\learning\\nextjs-navilaz\\pages\\courses\\professional-makeup.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




function professionalMakeup() {
  return __jsx(_Layout_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    style: {
      padding: '0 8%',
      textAlign: 'center'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    xl: 8,
    lg: 12,
    md: 24,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, __jsx("img", {
    src: "/assets/images/courses/professionalmakeup.webp",
    width: "100%",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  })), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    lg: 12,
    md: 24,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, __jsx(_components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__["Headings"], {
    fontsize: "2.4rem",
    paddingLeft: "2rem",
    fontweight: "700",
    paddingTop: "4rem",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "Professional Makeup Course"), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }), __jsx(_components_Services_LeftRightImages__WEBPACK_IMPORTED_MODULE_3__["Headings"], {
    fontsize: "2rem",
    paddingLeft: "2rem",
    fontweight: "600",
    paddingTop: "4rem",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, "About the Course"), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }), __jsx("p", {
    style: {
      textAlign: 'justify',
      marginLeft: '2rem',
      fontSize: '1.4rem',
      fontWeight: '600'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, "The professional makeup courses at Navilaz Studio ranges from 4 to 12 weeks and include all professional makeup disciplines from beauty makeup to advanced creative makeup as well as fashion, bridal, airbrush makeup courses. In addition the Navilaz Makeup Schools offer Professional Hairstyling Courses to ensure that our graduated students can offer full service makeup and hairstyling services to their future clients.")), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    span: 6,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, __jsx(Counter, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  })), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    span: 6,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    span: 6,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  })));
}

/***/ })

})
//# sourceMappingURL=professional-makeup.js.6a246d498b050cfecbc8.hot-update.js.map