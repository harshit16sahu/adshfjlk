webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/fixedComponents/Navbar.js":
/*!**********************************************!*\
  !*** ./components/fixedComponents/Navbar.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Navbar; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_10__);







var _jsxFileName = "C:\\Users\\harsh\\Desktop\\learning\\nextjs-navilaz\\components\\fixedComponents\\Navbar.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement;





var SubMenu = antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].SubMenu;
var Links = Object(styled_components__WEBPACK_IMPORTED_MODULE_9__["default"])(next_link__WEBPACK_IMPORTED_MODULE_10___default.a).withConfig({
  displayName: "Navbar__Links",
  componentId: "sc-5rerle-0"
})(["&:hover{color:red !important;border-bottom:hidden;font-size:2rem !important;}"]);
var MenuItem = Object(styled_components__WEBPACK_IMPORTED_MODULE_9__["default"])(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item).withConfig({
  displayName: "Navbar__MenuItem",
  componentId: "sc-5rerle-1"
})(["border-bottom:hidden !important;"]);
var MyDrawerButton = Object(styled_components__WEBPACK_IMPORTED_MODULE_9__["default"])(antd__WEBPACK_IMPORTED_MODULE_8__["Button"]).withConfig({
  displayName: "Navbar__MyDrawerButton",
  componentId: "sc-5rerle-2"
})(["position:absolute !important;top:1.5rem;right:1rem;@media (min-width:1200px){display:none !important;}"]);
var DesktopRouter = Object(styled_components__WEBPACK_IMPORTED_MODULE_9__["default"])(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"]).withConfig({
  displayName: "Navbar__DesktopRouter",
  componentId: "sc-5rerle-3"
})(["border-bottom:hidden;@media (max-width:1200px){display:none;font-weight:900;}"]);
var AnchorTab = styled_components__WEBPACK_IMPORTED_MODULE_9__["default"].a.withConfig({
  displayName: "Navbar__AnchorTab",
  componentId: "sc-5rerle-4"
})(["color:black !important;font-size:1.4rem;font-weight:600;"]);
var SubmenuItems = styled_components__WEBPACK_IMPORTED_MODULE_9__["default"].a.withConfig({
  displayName: "Navbar__SubmenuItems",
  componentId: "sc-5rerle-5"
})(["color:black !important;font-size:1rem;font-weight:400;"]);

var Navbar =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(Navbar, _Component);

  function Navbar() {
    var _getPrototypeOf2;

    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Navbar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Navbar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "state", {
      visible: false
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "showDrawer", function () {
      _this.setState({
        visible: true
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "onClose", function () {
      _this.setState({
        visible: false
      });
    });

    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Navbar, [{
    key: "render",
    value: function render() {
      return __jsx(react__WEBPACK_IMPORTED_MODULE_7___default.a.Fragment, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Row"], {
        type: "flex",
        align: "middle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
        lg: 4,
        xl: 4,
        xs: 24,
        sm: 24,
        md: 24,
        style: {
          textAlign: 'center'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }, __jsx("a", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, __jsx("img", {
        src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAjVBMVEX///8jHyAAAAAYExRta2seGhv8/Pycm5sOBgl0c3RJRkejoqKzsrL39/cgHB0IAAC9vb3ExMTh4eGNi4vx8fHNzMzq6uoUDhCpqKgaFReWlZWCgYEOBQjU1NTa2tqgn580MTJBPj8oJCVSUFBeXF1ubG23trZ4d3cvKyxaWFlNSks8ODmOjY1XVFVmY2TGrz0WAAAL60lEQVR4nO1dDXeqOBA1oYgWCYoIovjRVmtbff7/n7cJKCSBBFAQ2JN7zp59WtC5EDJzJzNxMFBQUFBQUFBQUFBQUFDoHKy2DWgas4+2LWgYbggvbdvQLK4+gNu2jWgSSwiAvh+2bUZz8CACAIxObdvRHP40QAC/2zakKaztiCBAtte2Kc1gA8EN2qFtWxqB9anfGQK4bNuaJnAcgRTQaduc+jGFFEEQvLdtT+3wbEQzBPa6bYvqxj8NsICLtk2qFwbkCAL9023bqDqxzRAEwH9r26o68R5kGQI4bdus+nAPZniK/5vQZpYzRv9XLsMNUD5DAI22basHB19AEFPctG1cHZgIxmg0Tj/+B2qYD2ZYjPof2gw/8hwFNU57r4Yv+Y4iAfJ7HtosJA9hDL/fatj60YsY9lwNr8NCggD1ObT5LhyjBD12GS6UOYoU9rxtSx/FL696ReirGl6VGqMEOuily3AKPCGNXib6h7mqVzhOJ23bWx0C1SsA6l8CVaR6RdD+tW1xRYhVrwh9U8NXseoVUuzV2rBM9YrQq9DGKRnMsAh75DIqOYoU/UmgXh4YowQI9SS02TxIsDdq2NoXq14R+hHanEqoXhF6EdqUU70iBOe27S+E5z/iKFJ0v+ztq6zqFVLseKLffGqMEug/na5AdZ4miNXwuG0WEhSl8MuhywnUOVa9SOcgmXn4QwnwJ4w6m0Alqhft/94Z/InT3p/coeToP9BhNRwVroUm/7ZQaKAc3zcc/uCj4e4V9lZHXLim2R/jKTMdTkXTD4I/1x01Ip3LRxhfjm6GNonq1TXoz+i/zIUzLAogXN2OssYwvA9o/aeDatijByNipd4/WU4DHuODznQ8G3ZwbZh1FOyCkvspkxuxdzix2cfuqWFe9bJZbKlkRMAiQ4B/s2NqOEMBfTKzTbZyj7tfS/6A0bEtLrnIUb1cBC1LL/rzweAtE7F3K7RZjzJmY7m+o27jUPIoBofB8Jz5c6fUcJ7HC43B6kodwz9oFPR3HC1kw4KgO01guaoXDz0j8XUE4iwx2g/cvHUc2Jm14dzCNf80MEJIe/6TaDUKfQ7z73BX1HC+6tXGA2OEdNot/gn0P9IFDDuihgWqN2IItHcq/PJQfgyOGQo+xO+Eyzjnq96YIdt1IFhUFDPsRNmbaK33xpB1a/nFCxKGKGxdDQsL1+4M2XXB37xJScKwfTUsLlxLGDKlJC7KOV7GEMCMpH4tjtlghmcIfPo2bHMUv5Rhyy5DstabMmTz2JkQu4ih/tMaPXmFM8WQnRGzjl/OsNUmMFl6lGbIBNHDjHspYNiiGpaKPpohCM6U488k34oYclrzdZCv9TIMwYhu4+IdfxFD4F+FRjSJoXytl2UIGJnBpTwKGbakhnNUr4QhW0PKrsEVM0R+C2q4aK2XZ4h8Kv5yGUVZzLANNezKVlzyGALtizqdeRRLMGRH+UuQG2BKGQJI+zVaVJZh+PLQpnitN8uQDTGPaYq7FEMdvNRlOGFhPUIOQ2RTMsPaJ46/FEOSFnkh9sVrvTkM2ayEk4R85Ri+tJyoTIVzHkO2rCsJ20syZGbjZlHcriViyE6J9xi8JMPXhTaWdCVJzpCNom/Jt7IMX7Y2fJIHM3KGyKaik5v6Ks3wRZXSJSucBQzZYufY8Zdn+JJdtEo4CilDENIVQVHyrTzDl6hhUeK6NENWKJDYqALDFzSBSVVvOYaMlWTlsQrDxtVw3iYllRkyaV78iVUYNu4yylc4SxiyMmMJKzFsWA2XcxSFDFmZMQ4rMWT8Td0QVjdVZcg4/uHer8IQaH+NEfRylqIfZMjIDGdUiWGDaliySUlVhuw2Ud9WtdLbplxGtXatAoZgxMyJ1Rjqn40QrNiuVcSQbTisWD7dTDlRiQ0gKjEEdBlD1QLxJtaG55X6erFnXg+W8u4ZurqvKkOk1V72Vkr10hhdClOqQaoUKhf5194ENqw4RrEK2BV3O9tJcql6G0Pda8MVgpm7BbMSZiePU3WGjEd9Hg/09YY4tiqWkvc87wOtKMG+RoLeqHK7lk4WGrIllZnDbmUMjzTb1LklSlnVSyEKPEtoLf/rYYaMu3kOuwfGaDzVrYvPjPMSDzFEek1quLzqTb4a/t2+ewwL5+Dobj/WElaTGi6zmxWFwIfwJxWp0zOEvnSQI5IifLDprR41XGY3K4rg4TJlFao3XR2lyxwkRSipIJahliawipuU+OvZdDr9xkj/N1vKZZd/mJoPtkcz+ZDHULjWmzEXZlEU0mrw4f7v59XwW/VNSl4L+8m14ZwqtI5Bfy6BupVuzdkNPNc3XEtfb9N4JrQRNw52CSh8WA1XVr0tQft9kGB11dsWHlXDV6j1BY8lUDeHt97g2sGmWgUFBQUFBQWFPDSyi4RrLicYaT7rO3q9ZJcnl7TI3i6jQ/BBZrxysohPmQ025v0v5IRpclx+U9OGT06YNoreWsQfESP691PUveknhHCfUHA3bxBeN0zeYMOsxw7jUyC8OPFR1nYP4XnmDizHiP5A/k3ePkavjtv8JMSea7DwINKiH94hpx2Ws41DMCHWPVmuSFK1Ib1qfuarWU4h9w7ZgCCgstFT+HFjYYQApL/u+KeDQKTstpDrPnChrmvkY67wPc3HnnUUPl0/9O6zqeU5V6frhpkNLNYjssadwEnqXj1IczJDcXH63Pc5uWD+7COGX1T/+8WuY4PlMSmEoi4o/9wsYebHYggRKlM7TbpkrZCmjj9YlGIZQpRdqV9Go/SczgobWEu30JFUuep68nUTjuFh7aOAe5aOGv17XOuEBmGYdszOoLD0ZzL3gc2vSbxHSp7KrH3otZRFj63BdURlzzmGmzm+Y/xgW0AQprno9DJbPvLTZU0Jw/fBb6Dv2fe+o1topV+/g/XUf52saPUp6VbmGJ62g69A58fKXteTrtFZ+qRYWjmGzik7hCdf3EDZ4qFVy26nR4sMeJR8H8vQ+hf16fFtV4advnVNx2tZhvMN2bBGo1cHzcyc9BXUVN92IpduAgHSYktZht/L2G4u74xH7r1NyaPsLMuQjMf5SP7begasa8uzYzQ41va9jYBleCBGrH0w4ia+f8F9+lnRRd3lGEbjGjtiW9JA4pBrXk9rwjg29Eu71fcwDOMbhGdtfjGWjNzowCG9wFeS4VvkX38DJClB/AtqK1S4MbRGKKbBMLzEFuI7xm18YOko9u1Tmno5hl5c5SBxl7EXrqtb73ibwrAHiMqQGIbAjKJn8qv33Fxzuo3cN3oGLMdweZ2QkNqEQBP9VCl+zuvbwP10NxE/2gGOwWmGi/ViFgFPfFzF5yb2kg4zGZRj+C/+zAW+SKINzfAzU99GC8fkJoxDUhFIMzzcJzs810Dukr7rOp4S50zMUYrh4n5R8GQyyi97qnOMJs/hIN4lGJrTlKGVzCLY+4ZcCGzaZLZnayTlDN34m07JRfnCj3eetvJCxPzk3pO9pSnDgUNmm3XKkPLCxBh27nZ9FBoLdorNMqSnEiMakulliybkPOt/NaZG2H3ydr5RFyuSGalR1HYeE5h5ME6+/v7G3gISeYsZ/kVjfpJyGo5QkNNbgYkzK4bLJ4PTK/18zWmjNpSrw3cs4HZ0wpMvXz3IqidsqU0t/Hnxk7ynJhfyeGfEg4unUTraGdpPjtI9Ezpd/ZThL7110JeecRhnnQ+7MgqYlpHHiOEMUsfja5D9betfnxm7w+uTnt9jnwTrM/m8CaRv2h5lFpsvkK/FXtoApUrzENDZgxUkZSPWD+0gSKUUf90wa3q0eL/hU7stuIs9hBOPmkO2N4buDtrwcrtHljOGtm3D64Ye0g57cYfeMjroI0pkDZ01eWXDt4XjOZvlB4SBZW3w133cEl1Dbxodb3/T3+9B/L0Lj8DZLowDefmECnZ3q51prgz6Is2ia+wahmmaxiqmuFyRV6aJD6cnVHYi3Rj3g0ySTbydgl8aOwP/Z5pL/OaOvBFPWdvVKj5itdrSnxKdRGCsotNMoxObKyooKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKJTGf22tzWfXV3uHAAAAAElFTkSuQmCC',
        height: "100",
        width: "100",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      })))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
        lg: 6,
        xl: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }), __jsx(DesktopRouter, {
        onClick: this.handleClick,
        selectedKeys: [this.state.current],
        mode: "horizontal",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }, __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, "Home"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/gallery",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }, "Gallery"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/testimonials",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, "Testimonials"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/services",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, " Services"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/contact",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }, " Contact"))), __jsx(SubMenu, {
        style: {
          position: 'relative',
          top: '0.4rem',
          borderBottom: 'hidden'
        },
        title: __jsx(AnchorTab, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 108
          },
          __self: this
        }, "Courses", __jsx("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 109
          },
          __self: this
        }), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
          type: "caret-down",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 110
          },
          __self: this
        })),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].ItemGroup, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:1",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/professional-makeup-course/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, " Professional Makeup Course "))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:2",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/self-makeup/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, " Self Makeup"))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/advanced-hairstyling/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, " Advanced Hairstyling"))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:4",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/advanced-makeup/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, " Advanced Makeup"))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:5",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/airbrush-makeup/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      }, " Air Brush Makeup")))))), __jsx("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      }, __jsx(MyDrawerButton, {
        onClick: this.showDrawer,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }, "Menu"), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Drawer"], {
        title: "Navilaz Studios",
        placement: "right",
        closable: false,
        onClose: this.onClose,
        visible: this.state.visible,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"], {
        onClick: this.handleClick,
        selectedKeys: [this.state.current],
        mode: "inline",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165
        },
        __self: this
      }, __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, " Home"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 171
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/gallery",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 172
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      }, " Gallery"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 176
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/testimonials",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      }, " Testimonials"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 181
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/services",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }, " Services"))), __jsx(MenuItem, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 186
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/contact",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      }, __jsx(AnchorTab, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 188
        },
        __self: this
      }, " Contact"))), __jsx(SubMenu, {
        style: {
          position: 'relative',
          top: '0.4rem',
          borderBottom: 'hidden'
        },
        title: __jsx(AnchorTab, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 194
          },
          __self: this
        }, "Courses"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].ItemGroup, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 197
        },
        __self: this
      }, __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:1",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 198
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/courses/professional-makeup",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 199
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 200
        },
        __self: this
      }, " Professional Makeup Course "))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:2",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/courses/self-makeup/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }, " Self Makeup"))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/courses/advanced-hairstyling/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }, " Advanced Hairstyling"))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:4",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 217
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/courses/advanced-makeup/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 218
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 219
        },
        __self: this
      }, " Advanced Makeup"))), __jsx(antd__WEBPACK_IMPORTED_MODULE_8__["Menu"].Item, {
        key: "setting:5",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 223
        },
        __self: this
      }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_10___default.a, {
        href: "/courses/airbrush-makeup/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 224
        },
        __self: this
      }, __jsx(SubmenuItems, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 225
        },
        __self: this
      }, " Air Brush Makeup"))))))))));
    }
  }]);

  return Navbar;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);



/***/ })

})
//# sourceMappingURL=index.js.0b27931e0fc8c256eb53.hot-update.js.map