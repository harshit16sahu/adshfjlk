import React, {Component} from 'react';
import { Skeleton, Switch, Card, Icon, Avatar } from 'antd';
import {Row, Col} from 'antd';
import styled from 'styled-components';
import Layout from '../Layout/Layout';
import Gallery from 'react-photo-gallery';


const { Meta } = Card;
const Cardview = styled.div`
    display:flex;
    flex-wrap:wrap;
    padding: 1% 5%;
`
const Cardspace = styled(Row)`
    padding: 1% 4%;
`
const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 5% 6%;
    justify-content:center;
    background:#72448d;
`

const TextWhiteCol = styled(Col)`
    color: white !important;
`

const Padding = styled(Col)`
    padding: 0 8%;
`
const PHOTO_SET_1 = [
    {
      src: '/assets/images/Gallery/1.jpg',
      width: 1,
      height: 1
    },
    {
      src: '/assets/images/Gallery/2.jpg',
      width: 1,
      height: 1
    },
    {
        src: '/assets/images/Gallery/3.jpg',
        width: 1,
        height: 1
      },
      {
        src: '/assets/images/Gallery/4.jpg',
        width: 1,
        height: 1
      },
  ];


  const PHOTO_SET_2 = [
    {
      src: '/assets/images/Gallery/6.jpg',
      width: 1,
      height: 1
    },
    {
      src: '/assets/images/Gallery/7.jpg',
      width: 1,
      height: 1
    },
    {
        src: '/assets/images/Gallery/8.jpg',
        width: 1,
        height: 1
      },
      {
        src: '/assets/images/Gallery/9.png',
        width: 1,
        height: 1
      },
  ];  

  const PHOTO_SET_3 = [
    {
      src: '/assets/images/Gallery/10.jpg',
      width: 1,
      height: 1
    },
    {
      src: '/assets/images/Gallery/11.jpg',
      width: 1,
      height: 1
    },
    {
        src: '/assets/images/Gallery/12.jpg',
        width: 1,
        height: 1
      },
      {
        src: '/assets/images/Gallery/13.png',
        width: 1,
        height: 1
      },
  ];  
  
class gallery extends Component {
    render() { 
        return ( 
            <Layout>
                <Wrapper>
            <Row style={{textAlign:"center"}}>
                <TextWhiteCol>
                    <p style={{fontSize: '3rem'}}>
                        Got A Question?
                    </p>
                    <p style={{fontSize: '1.75rem'}}>
                        Schedule a session? Contact directly
                    </p>
                
                </TextWhiteCol>
            </Row>
            
            
        </Wrapper>

       <Padding>
        <Gallery photos={PHOTO_SET_1} />
        <Gallery photos={PHOTO_SET_2} />
        <Gallery photos={PHOTO_SET_3} />

       </Padding>
            </Layout>
         );
    }
}
 
export default gallery;