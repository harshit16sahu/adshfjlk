import React, {Component} from 'react'
import styled from 'styled-components';
import { Row, Col, Button } from 'antd';
import { Input } from 'antd';
import Layout from '../Layout/Layout';

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 5% 6%;
    justify-content:center;
    background:#72448d;
    
`
const Wrapper1 = styled(Col)`
    padding:0% 8%;
`
const { TextArea } = Input;
const Pad = styled(Col)`
    padding: 0% 8%;   
`

const TextWhiteCol = styled(Col)`
    color: white !important;

`
class Contact extends Component {
        render(
        ) { 
        return (
         <Layout>  
         <Wrapper>
            <Row style={{textAlign:"center"}}>
                <TextWhiteCol>
                    <p style={{fontSize: '3rem'}}>
                        Got A Question?
                    </p>
                    <p style={{fontSize: '1.75rem'}}>
                        Schedule a session? Contact directly
                    </p>
                
                </TextWhiteCol>
            </Row>
            
            
        </Wrapper>
        <br/><br/>
        <Row style={{padding:'0 8%'}}>
           
            <Pad lg={12} xl={12} sm={24} xs={24} md={24} style={{ textAlign:"center"}}>
                        <h4>Send us a message asking for an appointment or to ask anything you want from our studio, you can receive beauty tips and tricks or schedule a session on our makeup studio to take care of yourself.
                     </h4>
                
            
                <Col style={{textAlign:'center'}}><br/><br/>
                    <Input placeholder="First Name" style={{width:"40%", marginRight: "3%",marginBottom:"3%"}}/>
                        <Input placeholder="Last Name" style={{width:"40%"}}/><br/>
                        <Input placeholder="Email" style={{width:"83%", marginBottom:"3%"}}/><br/>
                        <TextArea placeholder="Message" rows={4} style={{width:"83%"}}/><br/>
                        <br/><Button type="primary" block  style={{width:"83%"}}>Send</Button>
                </Col>

            </Pad>

            <Pad lg={12} xl={12} sm={24} xs={24} md={24} style={{textAlign:'center'}} >
                {/* <img src={require("../../images/contact.png")}
                width="200" height="150"/>*/}
                <h2>Contact Us</h2>
                {/* <h4>
                    <span style={{fontWeight:'bold'}}>Call   </span>: +91 77609 66452<br/><br/>
                    <span style={{fontWeight:'bold'}}>Timing </span>: 10am-5:30pm<br/><br/>
                    <span style={{fontWeight:'bold'}}>Address</span>: 3rd Floor, 27th Main Road,<br/> HSR Layout, Sectior 1,<br/> Banglore 560102<br/><br/>
                    <span style={{fontWeight:'bold'}}>Email </span>:@navilazstudio.com
                </h4><br/> */}
                <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Call:</span>
                </Col>
                <Col span={14} style={{textAlign:'left'}}>
                    <span style={{paddingLeft:4}}></span>+91 77609 66452<br/><br/>
                </Col>
                
                

                <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Timings:</span>
                </Col>
                <Col span={14} style={{textAlign:'left'}}>
                    <span style={{paddingLeft:4}}></span>10am-5:30pm<br/><br/>
                </Col>

                
                <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Address:</span>
                </Col>
                <Col span={14} style={{textAlign:'left'}}>
                    <span style={{paddingLeft:4}}></span>3rd Floor, 27th Main Road,<br/> HSR Layout, Sectior 1,<br/> Banglore 560102<br/><br/>
                </Col>
                

                <Col span={6}></Col>
                <Col span={4} style={{textAlign:'left'}}>
                <span style={{fontWeight:'bold'}}>Email:</span>
                </Col>
                <Col span={14} style={{textAlign:'left'}}>
                    <span style={{paddingLeft:4}}></span>contact@navilazstudio.com<br/><br/>
                </Col>

                <Col span={24}>
                <iframe 
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.9303940286686!2d77.64947031460282!3d12.91219521967317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae149d3cffed67%3A0x5ee43d11b4411523!2sINIFD+Bangalore+HSR+Layout!5e0!3m2!1sen!2sin!4v1548012056385" 
                        frameborder="0" 
                        style={{border: '0px', pointerEvents: 'none'}} 
                        allowfullscreen=""></iframe>
                </Col>
            </Pad>  

        </Row>
            
        
        </Layout>
        
        );
    }
}
 
export default Contact;