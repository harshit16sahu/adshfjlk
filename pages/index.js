import Layout from '../Layout/Layout';
import HomePage from '../components/Homepage/Homepage';
import Navbar from '../components/fixedComponents/Navbar';
export default function Index() {
  return (
    <div>
        <Layout>
            <HomePage />
        </Layout>
    </div>  
    
  );
}