import React from 'react';
import Layout  from '../../Layout/Layout';
import {Row,Col} from 'antd'
import {Headings} from '../../components/Services/LeftRightImages'
import Counter from '../../components/Homepage/ClientCounter'
import Accordion from '../../components/courses/Accordion';
export default function advancedMakeup() {
    return (
        <Layout >
          <div >
            <Row style={{padding:'0 8%', textAlign:'center', background: '#f3f3f3'}}>
                <Col xl={8} lg={12} md={24}>
                    <img src="/assets/images/courses/advanced-makeup.jpg" 
                        width="100%"
                        height = "auto"
                    />

                </Col>
                <Col lg={12} md={24}>
                    <Headings fontsize="2.4rem" paddingLeft="2rem" fontweight="700" paddingTop="0">
                        Advanced Makeup
                    </Headings>
                    <br/>
                    <Headings fontsize="2rem" paddingLeft="2rem" fontweight="600" paddingTop="0">
                        About the Course</Headings>
                    <br />
                    <br />
                    <p style={{textAlign:'justify', marginLeft: '2rem', fontSize: '1.4rem', fontWeight: '600'}}>
                        Makeup Courses gives you the platform to become a Professional Makeup Artist by learning from the best in the Beauty Industry. We give a certificate of completion to our students upon completion. These courses encompasses learning about the Makeup Theory, the makeup tools involved and high demand Makeup like Engagement Makeup.
                        
                    </p>
                </Col>
                
            </Row>   
            <Row style={{padding:'0 18%'}}>
            <Headings style={{margin:'auto'}} fontsize="2rem" fontweight= "600">Our Curriculum</Headings>
            <br/>
            <br/>
            <Col xl={12} md={24} style={{textAlign:'left'}}>
            <ul>
                <li>Understanding Makeup Theory</li>
                <li>Skin Type Analysis</li>
                <li>Preparing the Face</li>
                <li>Tools of Trade/Brush Kits</li>
                <li>Hygiene</li>
                <li>The Color Theory</li>
                <li>Textures &amp; Mediums</li>
                <li>Understanding Skin Tones</li>
                <li>Foundations</li>
                <li>Powders</li>
                <li>Concealers</li>
                <li>Highlighting&nbsp; &amp; Contouring</li>
                <li>Eye Makeup</li>
                <li>Fashion/Runaway Makeup</li>
                <li>Photo-Shoot</li>
                <li>Glossy Look</li>
                <li>HD Bridal Makup</li>
            </ul>
            </Col>
            <Col xl={12} md={24} style={{textAlign:'left'}}>
            <ul>
                <li>Eye-makeup</li>
                <li>Eyeliner &amp; Kohl</li>
                <li>False eyelash Application</li>
                <li>Lip Liner</li>
                <li>Lip Textures</li>
                <li>Blusher &amp; Bronze</li>
                <li>Smokey Eyes</li>
                <li>Shimmer Makeup</li>
                <li>Pigment Application</li>
                <li>Lips &amp; Cheeks</li>
                <li>Advantage foundation Techniques</li>
                <li>Advance Bridal Makeup</li>
                <li>Indian Traditional Makeup</li>
                </ul>
            </Col>
            </Row>
            <br/>
            <br/>  
            <Row style={{padding:'0 18%', textAlign:''}}>
                <Headings fontsize="2rem" fontweight= "600">Some Frequently Asked Questions about this course.</Headings>
                <br />
                <Accordion header= "What is the duration of the course?" text="The Duration of this course is 36 days.

The timings will be Monday to Saturday 2:00pm-3:00pm"/> 

<Accordion header= "Will I get a Certificate for this Course?" text="Yes, you will be receiving a diploma certificate from us which is Internationally recognized. Absolutely worth your time and effort, the certificate will provide you with International recognition where you can showcase your talent in Makeup."/> 
<Accordion header= "Do you offer professional photo-shoot for students" text="Yes, 2 professionally shot portfolio shoots, from which students can start building their portfolio. The photoshoot will immensely impact your career and our trained photographers will be working alongside you."/> 

            </Row>
           <br/>
           <br />
            </div>
        </Layout>
    )
}
