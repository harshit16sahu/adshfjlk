import React from 'react';
import Layout  from '../../Layout/Layout';
import {Row,Col} from 'antd'
import {Headings} from '../../components/Services/LeftRightImages'
import Counter from '../../components/Homepage/ClientCounter'
import Accordion from '../../components/courses/Accordion';
export default function professionalMakeup() {
    return (
        <Layout >
          <div >
            <Row style={{padding:'0 8%', textAlign:'center', background: '#f3f3f3'}}>
                <Col xl={8} lg={12} md={24}>
                    <img src="/assets/images/courses/professionalmakeup.webp" 
                        width="100%"
                    />

                </Col>
                <Col lg={12} md={24}>
                    <Headings fontsize="2.4rem" paddingLeft="2rem" fontweight="700" paddingTop="0">
                        Professional Makeup Course
                    </Headings>
                    <br/>
                    <Headings fontsize="2rem" paddingLeft="2rem" fontweight="600" paddingTop="0">
                        About the Course</Headings>
                    <br />
                    <br />
                    <p style={{textAlign:'justify', marginLeft: '2rem', fontSize: '1.4rem', fontWeight: '600'}}>
                        The professional makeup courses at Navilaz Studio ranges from 4 to 12 weeks and include all professional makeup disciplines from beauty makeup to advanced creative makeup as well as fashion, bridal, airbrush makeup courses. In addition the Navilaz Makeup Schools offer Professional Hairstyling Courses to ensure that our graduated students can offer full service makeup and hairstyling services to their future clients.
                    </p>
                </Col>
                
            </Row>   
            <br/>
            <br/>  
            <Row style={{padding:'0 18%', textAlign:''}}>
                <Headings fontsize="2rem" fontweight= "600">Some Frequently Asked Questions about this course.</Headings>
                <br />
                <Accordion header= "What is the duration of the course?" text="The Duration of this course is 36 days.

The timings will be Monday to Saturday 2:00pm-3:00pm"/> 

<Accordion header= "Will I get a Certificate for this Course?" text="Yes, you will be receiving a diploma certificate from us which is Internationally recognized. Absolutely worth your time and effort, the certificate will provide you with International recognition where you can showcase your talent in Makeup."/> 
<Accordion header= "Do you offer professional photo-shoot for students" text="Yes, 2 professionally shot portfolio shoots, from which students can start building their portfolio. The photoshoot will immensely impact your career and our trained photographers will be working alongside you."/> 

            </Row>
            </div>
        </Layout>
    )
}
