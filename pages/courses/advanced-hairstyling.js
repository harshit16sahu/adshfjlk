import React from 'react';
import Layout  from '../../Layout/Layout';
import {Row,Col} from 'antd'
import {Headings} from '../../components/Services/LeftRightImages'
import Counter from '../../components/Homepage/ClientCounter'
import Accordion from '../../components/courses/Accordion';
export default function advancedHairstyling() {
    return (
        <Layout >
          <div >
            <Row style={{padding:'0 8%', textAlign:'center', background: '#f3f3f3'}}>
                <Col xl={8} lg={12} md={24}>
                    <img src="/assets/images/courses/hairstyling.jpg" 
                        width="100%"
                        height = "auto"
                    />

                </Col>
                <Col lg={12} md={24}>
                    <Headings fontsize="2.4rem" paddingLeft="2rem" fontweight="700" paddingTop="0">
                        Advanced Hairstyling
                    </Headings>
                    <br/>
                    <Headings fontsize="2rem" paddingLeft="2rem" fontweight="600" paddingTop="0">
                        About the Course</Headings>
                    <br />
                    <br />
                    <p style={{textAlign:'justify', marginLeft: '2rem', fontSize: '1.4rem', fontWeight: '600'}}>
                    This course concentrates on the Advanced hairstyling, from simple blow-drying techniques to creating elaborate hairstyles. You will learn to create modern styles, as well as edgy and creative hair designs, using hot tools and roller sets. This course incorporates daily projects and skill-building sessions to encourage learning by practice. As part of this course, students are required to complete hair and make-up projects, which relate to on-the-job experience.
                    </p>
                </Col>
                
            </Row>   
            <br/>
            <br/>  
            <Row style={{padding:'0 18%'}}>
            <Headings style={{margin:'auto'}} fontsize="2rem" fontweight= "600">Our Curriculum</Headings>
            <br/>
            <br/>
            <Col xl={12} md={24} style={{textAlign:'left'}}>
                <ul>
                    <li>Introduction to hair kit and its types</li>
                    <li>Hygiene and client handling</li>
                    <li>Sectioning</li>
                    <li>Introduction to electronic tools</li>
                    <li>Blow-dry and blow dry-curls</li>
                    <li>Volume Blow-dry</li>
                    <li>Soft Curls</li>
                    <li>Straightening</li>
                    <li>Incurls</li>
                </ul>
            </Col>
            <Col xl={12} md={24} style={{textAlign:'left'}}>
                <ul>
                    <li>Introduction to hair kit and its types</li>
                    <li>Hygiene and client handling</li>
                    <li>Sectioning</li>
                    <li>Introduction to electronic tools</li>
                    <li>Blow-dry and blow dry-curls</li>
                    <li>Volume Blow-dry</li>
                    <li>Soft Curls</li>
                    <li>Straightening</li>
                    <li>Incurls</li>
                </ul>
            </Col>
            </Row>
        <br/>
        <br/>

            <Row style={{padding:'0 18%', textAlign:''}}>
                <Headings fontsize="2rem" fontweight= "600">Some Frequently Asked Questions about this course.</Headings>
                <br />
                <br />
                <Accordion header= "What is the duration of the course?" text="The Duration of this course is 36 days.

The timings will be Monday to Saturday 2:00pm-3:00pm"/> 

<Accordion header= "Will I get a Certificate for this Course?" text="Yes, you will be receiving a diploma certificate from us which is Internationally recognized. Absolutely worth your time and effort, the certificate will provide you with International recognition where you can showcase your talent in Makeup."/> 
<Accordion header= "Do you offer professional photo-shoot for students" text="Yes, 2 professionally shot portfolio shoots, from which students can start building their portfolio. The photoshoot will immensely impact your career and our trained photographers will be working alongside you."/> 
<br/>
<br/>
            </Row>
            </div>
        </Layout>
    )
}
