import React from 'react';
import Layout  from '../../Layout/Layout';
import {Row,Col} from 'antd'
import {Headings} from '../../components/Services/LeftRightImages'
import Counter from '../../components/Homepage/ClientCounter'
import Accordion from '../../components/courses/Accordion';
export default function professionalMakeup() {
    return (
        <Layout >
          <div >
            <Row style={{padding:'0 8%', textAlign:'center', background: '#f3f3f3'}}>
                <Col xl={8} lg={12} md={24}>
                    <img src="/assets/images/courses/airbrush-makeup.jpg" 
                        width="100%"
                    />

                </Col>
                <Col lg={12} md={24}>
                    <Headings fontsize="2.4rem" paddingLeft="2rem" fontweight="700" paddingTop="0">
                        Professional Airbrush Makeup 
                    </Headings>
                    <br/>
                    <Headings fontsize="2rem" paddingLeft="2rem" fontweight="600" paddingTop="0">
                        About the Course</Headings>
                    <br />
                    <br />
                    <p style={{textAlign:'justify', marginLeft: '2rem', fontSize: '1.4rem', fontWeight: '600'}}>
                    Airbrushing has become one of the premier ways of applying make-up. In this course, the airbrush technique is de-mystified. You will be shown how to properly care for and maintain your airbrush. In addition, students will study the techniques necessary to do beautiful and flawless make-up. Learn the techniques that are being required by major studios around the world as well as clients.
                    </p>
                </Col>
                
            </Row>  
<br />
            <Row style={{padding:'0 18%'}}>
            <Headings style={{margin:'auto'}} fontsize="2rem" fontweight= "600">Our Curriculum</Headings>
            <br/>
            <br/>
            <Col xl={12} md={24} style={{textAlign:'left'}}>
            <ul>
                <li>Advantages of Airbrush Make-up</li>
                <li>Airbrush Equipment&nbsp; knowledge</li>
                <li>Formulation</li>
                <li>Assembling the Airbrush Gun</li>
                <li>Back Bubbling</li>
                <li>Airbrush Techniques and Hands-on Practise</li>
                <li>Eye Make-up</li>
                <li>Linear Application</li>
                <li>Lipstick Application with Airbrush</li>
                <li>Full Face Makeup with Airbrush</li>
                <li>Face Charts with Airbrush</li>
                <li>Pro Bridal Airbrush Make-up</li>
                </ul>
            </Col> 
            </Row>
            <br/>
            <br/>  
            <Row style={{padding:'0 18%', textAlign:''}}>
                <Headings fontsize="2rem" fontweight= "600">Some Frequently Asked Questions about this course.</Headings>
                <br />
                <br />
                <Accordion header= "What is the duration of the course?" text="The Duration of this course is 36 days.

The timings will be Monday to Saturday 2:00pm-3:00pm"/> 

<Accordion header= "Will I get a Certificate for this Course?" text="Yes, you will be receiving a diploma certificate from us which is Internationally recognized. Absolutely worth your time and effort, the certificate will provide you with International recognition where you can showcase your talent in Makeup."/> 
<Accordion header= "Do you offer professional photo-shoot for students" text="Yes, 2 professionally shot portfolio shoots, from which students can start building their portfolio. The photoshoot will immensely impact your career and our trained photographers will be working alongside you."/> 
            <br />
            <br />
            </Row>
            </div>
        </Layout>
    )
}
