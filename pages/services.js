import React, {Component} from 'react';

import {Row, Col} from 'antd';
import styled from 'styled-components';
import Layout from '../Layout/Layout';
import { Divider } from 'antd';
import OurServices from '../components/Homepage/OurServices';
import LeftRightImages from '../components/Services/LeftRightImages';
import ContactNavila  from '../components/Services/ContactNavila'
const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 5% 6%;
    justify-content:center;
    background:#72448d;
`

const TextWhiteCol = styled(Col)`
    color: white !important;
`
const ImageResponsive = styled.img`
    @media (min-width: 1200px){
        height: 450px;
    }
`
const DividerStyling = styled(Divider)`

`
class services extends Component {
    render() { 
        return ( 
            <Layout>
                <Wrapper>
            <Row style={{textAlign:"center"}}>
                <TextWhiteCol>
                    <p style={{fontSize: '3rem'}}>
                        The best beauty academy
                    </p>
                    <p style={{fontSize: '1.75rem'}}>
                        SERVICES & TREATMENTS WITH PROFESSIONALS IN BEAUTY
                    </p>
                
                </TextWhiteCol>
            </Row>
            </Wrapper>
            <br/>
            <Row style={{paddingLeft: 30, paddingRight: 30, margin:'auto'}}>
                
                <Col xl={12} md={22} style={{textAlign:'center'}}>
                    <ImageResponsive src="/assets/images/bridal-makeup-service.webp"
                        width="90%" 
                    />
                </Col>
                <Col xl={12} md={24}>
                        <p style={{fontSize: '3rem', fontWeight:'700'}}>Bridal Service</p>
                    

                    <h1>Wedding is the most important day of our life</h1>
                    <span style={{fontSize: '1.3rem'}}>
                    Let the professionals at Navilaz Studio assist you in helping you look and feel your best. Begin your “happily ever after” from head to toe by ensuring you cover each specific detail.
                    We offer a full range of beauty services to everyone located in the area. Our professionals know how to handle a wide range of treatments.
                    </span>
                </Col>
            </Row>

            <br/>
            <br/>
            <OurServices />
            <br/>
            <br/>
           <LeftRightImages />
           <br/>
           
           <br/>
           <ContactNavila />
       
            </Layout>
         );
    }
}
 
export default services;