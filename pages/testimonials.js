import React, {Component} from 'react';
import { Skeleton, Switch, Card, Icon, Avatar } from 'antd';
import {Row, Col} from 'antd';
import styled from 'styled-components';
import Layout from '../Layout/Layout';

const { Meta } = Card;
const Cardview = styled.div`
    display:flex;
    flex-wrap:wrap;
    padding: 1% 5%;
`
const Cardspace = styled(Row)`
    padding: 1% 4%;
`
const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 5% 6%;
    justify-content:center;
    background:#72448d;
`

const TextWhiteCol = styled(Col)`
    color: white !important;

`
class clients extends Component {
    state = {
        loading: false,
      };
       
    render() { 
        const { loading } = this.state;
        return ( 
            <Layout>

        <Wrapper>
            <Row style={{textAlign:"center"}}>
                <TextWhiteCol>
                    <p style={{fontSize: '3rem'}}>
                       What our clients say?
                    </p>
                    <p style={{fontSize: '1.75rem'}}>

                    </p>
                </TextWhiteCol>
            </Row>
        </Wrapper>
                <Cardview>
                    <Cardspace>
                    <Card style={{ width: 300, marginTop: 16 }} loading={loading}>
                        <Meta style = {{textAlign:'justify',fontStyle:'italic'}}
                            avatar={
                            <Avatar src="/assets/images/clients/dulu.jpg" />}
                                title="Asmita Samantha"
                                description="The success and final outcome of this course required a lot of guidance and assistance from many people and I am extremely privileged to have got this all along the completion of my course. All that I have done is only due to such supervision and assistance and I would not forget to thank them. "    
                        />
                    </Card>
                    </Cardspace>
                    <Cardspace>
                    <Card style={{ width: 300, marginTop: 16 }} loading={loading}>
                        <Meta style = {{textAlign:'justify',fontStyle:'italic'}}
                            avatar={
                            <Avatar src="/assets/images/clients/kiranjeetkaur.jpg" />}
                                title="Kiranjeet Kaur"
                                description="I had excellent experience at NavilazStudio. My interest in make-up came to a different level. Our trainer gave us a cozy atmosphere. I have accumulated lot of good memories in this Studio. All the staff members were very co-operative overall it was great experience as a student here."                        />
                    </Card>
                    </Cardspace>
                    <Cardspace>
                    <Card style={{ width: 300, marginTop: 16 }} loading={loading}>
                        <Meta style = {{textAlign:'justify', fontStyle:'italic'}}
                            avatar={
                            <Avatar src="/assets/images/clients/pratiksha.jpg" />}
                                title="Anindita"
                                description="I am very satisfy I am very happy end of the course. My experience was very fantastic. I would not forget to number Make-Up Studio Training Center for their encouragement and more own for their support and guidance till the Completion of my course"                        />
                    </Card>
                    </Cardspace>
                    <Cardspace>
                    <Card style={{ width: 300, marginTop: 16 }} loading={loading}>
                        <Meta style = {{textAlign:'justify',fontStyle:'italic'}}
                            avatar={
                            <Avatar src="/assets/images/clients/asmita.jpg" />}
                                title="Palashi"
                                description="I had a very good experiencein NavilazStudio I came to learn so many new things here. I came to know so many things about the product. Everyone here is very cooperative and nice. The best thing about NavilazStudio is they teach us very well and we have gained so much a knowledge regarding make-up and products"                        />
                    </Card>
                    </Cardspace>
                    <Cardspace>
                    <Card style={{ width: 300, marginTop: 16 }} loading={loading}>
                        <Meta style = {{textAlign:'justify',fontStyle:'italic'}}
                            avatar={
                            <Avatar src="/assets/images/clients/something.jpg" />}
                                title="Rajni"
                                description="I have learned a lot at NavilazStudio, now I can literally feel that I have really become a make-up artist & for others rest all depends about ones attitude way of thinking and of course your artists hand and practice. It is the most element to practice makes the man perfects. "                        />
                    </Card>
                    </Cardspace>
                </Cardview>
            </Layout>
         );
    }
}
 
export default clients;